

## Prerequisites

Make sure you have the following installed locally before continuing:

- Node.js v18.x
- [nvm](https://github.com/nvm-sh/nvm)
- Yarn v3.4 (please note, v4 is not supported)

## Initial setup

1. Clone this repo
1. Run `nvm use`
1. Run `yarn install`
1. Create `.env` file
1. Run `yarn secret` and copy the value to the `SESSION_SECRET` variable in `.env`
1. Run `yarn migrate`
1. Run `yarn dev`

### Troubleshooting


## Instructions



#### Feature Request

Implement a stepper as proposed in the design (import `./stepper.fig` into Figma so you can inspect the design)

- [ ] Step 2 should:
  1. [ ] display a list of options
  1. [ ] allow users to select only one option
  1. [ ] extra content (purple box  the right) should only be shown when the `View more` button is clicked
  1. [ ] a `View less` option should be shown when the extra content is visible
  1. [ ] mobile view (small devices) should render the extra content below each option instead of to the right
  1. [ ] the `Next` button should be disabled until an option is selected the checkbox is checked

# Redwood

## Getting Started
- [Tutorial](https://redwoodjs.com/tutorial/welcome-to-redwood): getting started and complete overview guide.
- [Docs](https://redwoodjs.com/docs/introduction): using the Redwood Router, handling assets and files, list of command-line tools, and more.
- [Redwood Community](https://community.redwoodjs.com): get help, share tips and tricks, and collaborate on everything about RedwoodJS.

### Setup

We use Yarn as our package manager. To get the dependencies installed, just do this in the root directory:

```terminal
yarn install
```

### Fire it up

```terminal
yarn redwood dev
```

Your browser should open automatically to `http://localhost:8910` to see the web app. Lambda functions run on `http://localhost:8911` and are also proxied to `http://localhost:8910/.redwood/functions/*`.
