export const Circle = ({
  state,
}: {
  state: 'active' | 'complete' | 'disabled'
}) => {
  const outerColorForState = {
    active: 'bg-indigo-700',
    complete: 'bg-[#0094FF]',
    disabled: 'bg-white',
  }

  const innerColorForState = {
    active: 'bg-white',
    complete: 'bg-white',
    disabled: 'bg-[#E5E7EB]',
  }

  return (
    <div
      className={`h-[24px] w-[24px] ${outerColorForState[state]} flex justify-center items-center rounded-full`}
    >
      <div
        className={`rounded-full h-[12px] w-[12px] ${innerColorForState[state]}`}
      />
    </div>
  )
}
