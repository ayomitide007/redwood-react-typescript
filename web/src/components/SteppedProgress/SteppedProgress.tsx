import { Circle } from './components'

type SteppedProgressProps = {
  steps: number[]
  activeStep: number
  subSteps?: number
  activeSubStep?: number
  mapText: (step: number) => string
  onNavigate?: (step: number) => void
  className?: string
}

export const SteppedProgress = (props: SteppedProgressProps) => {
  const {
    steps,
    activeStep,
    subSteps,
    activeSubStep,
    mapText,
    onNavigate,
    className,
  } = props

  const Step = ({ step, idx }: { step: number; idx: number }) => {
    return (
      <div className="flex items-center">
        {idx !== 0 && (
          <div
            className={`h-[2px] w-[100px] sm:w-[220px] md:w-[280px] lg:w-[318px] ${
              idx <= steps.indexOf(activeStep) ? 'bg-[#0094FF]' : 'bg-[#E5E7EB]'
            } ${className}`}
          />
        )}
        <div key={idx} className="relative flex justify-center">
          <Circle
            state={
              step === activeStep
                ? 'active'
                : idx < steps.indexOf(activeStep)
                ? 'complete'
                : 'disabled'
            }
          />
          <div
            className={`absolute top-[100%] flex justify-center w-max ${
              steps.indexOf(step) < steps.indexOf(activeStep)
                ? 'text-[#0094FF]'
                : 'text-[#7B7B8C]'
            }`}
          >
            <div className="flex flex-col items-center">
              <div className="text-center mt-1">{mapText(step)}</div>
              {step === activeStep && subSteps && (
                <div className="text-sm text-gray-500">
                  {activeSubStep} of {subSteps}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }

  const LinkedStep = ({ step, idx }: { step: number; idx: number }) => {
    if (onNavigate) {
      return (
        <button
          onClick={() => onNavigate(step)}
          aria-label={`Step: ${mapText(step)}`}
        >
          <Step step={step} idx={idx} />
        </button>
      )
    } else {
      return <Step step={step} idx={idx} />
    }
  }

  return (
    <div className="w-full flex items-center justify-center">
      {(steps ?? []).map((step, idx) => (
        <LinkedStep key={idx} step={step} idx={idx} />
      ))}
    </div>
  )
}
