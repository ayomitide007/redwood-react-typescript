import React from 'react'

const AIDetails = () => {
  return (
    <div className="investment-status-info">
      <div className="pb-4 border-solid border-b border-[#C4C4C4]">
        <h6 className="mb-[5px]">
          What qualifies you as an Accredited Investor?
        </h6>

        <a href="/" className="mt-[5px] block">
          Please see the SEC website for more information.
        </a>
      </div>

      <div className="pt-4">
        <h6 className="mb-[5px]">Entities with Assets Exceeding $5 Million</h6>
        <p>
          Any organization, whether a corporation, Massachusetts or similar
          business trust, or partnership, not formed for the specific purpose of
          acquiring the securities being offered, with total assets in excess of
          $5 million qualifies.
        </p>

        <h6 className="my-[6px]">Registered Investment Companies</h6>
        <p>
          Any investment company registered under the Investment Company Act of
          1940 automatically qualifies as an accredited investor.
        </p>

        <h6 className="my-[6px]">Private Business Development Companies</h6>
        <p>
          Any private business development company as defined in section
          2(a)(48) of the Investment Company Act of 1940 is also considered an
          accredited investor.
        </p>

        <h6 className="my-[6px]">Individuals</h6>
        <p>
          An individual qualifies as an "accredited investor" in the U.S. if
          they've had an annual income of $200,000 (or $300,000 jointly with a
          spouse or spousal equivalent) for the past two years with an
          expectation of the same in the current year, or if they have a net
          worth exceeding $1 million (including a spouse&apos;s assets but
          excluding the primary residence&apos;s value beyond its indebtedness).
          Additionally, individuals with certain professional certifications
          like the Series 7, Series 65, or Series 82 licenses, when in good
          standing, also qualify.
        </p>
      </div>
    </div>
  )
}

export default AIDetails
