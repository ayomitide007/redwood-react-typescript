import React from 'react'

const QPDetails = () => {
  return (
    <div className="investment-status-info">
      <div className="pb-4 border-solid border-b border-[#C4C4C4]">
        <h6 className="mb-[5px]">What qualifies you as QP?</h6>

        <p>
          As defined in Section 2(a)(51) Investment Company Act, a ‘‘Qualified
          purchaser’’ means
        </p>
        <a href="/" className="mt-[5px] block">
          Please see the SEC website for more information.
        </a>
      </div>

      <div className="pt-4">
        <h6 className="mb-4">Other qualifiers</h6>

        <h6 className="mb-[5px]">Investment Managers</h6>
        <p>
          Any person acting for its own account or the accounts of other
          Qualified Purchasers, who in the aggregate owns and invests on a
          discretionary basis, not less than $25 million in investments.
        </p>

        <h6 className="my-[6px]">Family Office</h6>
        <p>
          Any trust not formed for the specific purpose of acquiring the
          securities being offered, whose trustee or other person authorized to
          make decisions concerning the trust, and each settlor or other person
          who has contributed assets to the trust, is a person described in the
          first or second category.
        </p>

        <h6 className="my-[6px]">Trust</h6>
        <p>
          An investment company registered under the Investment Company Act,
          acting for its own account or for the accounts of other qualified
          institutional buyers, that is part of a family of investment companies
          (as defined in Rule 144A) which own in the aggregate at least
          $100,000,000 in Eligible Securities.
        </p>

        <h6 className="my-[6px]">Natural Person</h6>
        <p>
          Any natural person (and any relative, spouse, or relative of the
          spouse of such natural person who has the same primary residence as
          such person) who owns not less than $5 million in investments.
        </p>
      </div>
    </div>
  )
}

export default QPDetails
