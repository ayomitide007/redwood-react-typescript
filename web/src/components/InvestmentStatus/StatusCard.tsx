import React, { FC, ReactNode } from 'react'

type Props = {
  id: string
  header: string
  listItems: string[]
  moreInfo: ReactNode
  handleSelectStatus: (id: string) => void
  handleViewMore: (id: string, isViewed: boolean) => void
  isSelected: boolean
  isMoreViewed: boolean
}

const StatusCard: FC<Props> = ({
  id,
  header,
  listItems,
  moreInfo,
  isSelected,
  isMoreViewed,
  handleSelectStatus,
  handleViewMore,
}) => {
  return (
    <div
      className={` border border-solid border-[#5021E9] rounded-md hover:bg-[#EBEBFD] transition text-left ${
        isSelected ? 'bg-[#EBEBFD]' : ''
      }`}
    >
      <div className="px-[18px] pt-3 pb-[18px] ">
        <h6 className="text-base text-[#5021E9] mb-3">{header}</h6>

        <ul className="pl-5 mb-6">
          {listItems?.map((item) => (
            <li
              id={item}
              className="list-disc text-sm text-[#7B7B8C]"
              key={item}
            >
              {item}
            </li>
          ))}
        </ul>

        <div className="flex justify-between">
          <button
            className="text-[#5021E9] text-sm"
            onClick={() => {
              handleViewMore(id, !isMoreViewed)
            }}
          >
            View {isMoreViewed ? 'less' : 'more'}
          </button>
          <button
            className={`btn ${
              isSelected ? 'btn__outline' : 'btn__filled'
            } px-[30px]`}
            onClick={() => {
              handleSelectStatus(id)
            }}
          >
            {isSelected ? 'Selected' : 'Select'}
          </button>
        </div>
      </div>

      {isMoreViewed && <div className="view-more">{moreInfo}</div>}
    </div>
  )
}

export default StatusCard
