import React from 'react'

const QIBDetails = () => {
  return (
    <div className="investment-status-info">
      <div className="pb-4 border-solid border-b border-[#C4C4C4]">
        <h6 className="mb-[5px]">What qualifies you as QIB?</h6>

        <p>
          As defined in paragraph (a) of Rule 144A under the Securities Act of
          1933, acting for its own account, the account of another QIB, or the
          account of a QP, which in the aggregate owns and invests on a
          discretionary basis not less than $100,000,000 in securities not
          affiliated with the entity. If the amount of eligible securities
          listed above is less than $100,000,000, you may a Qualified
          Institutional Buyer if any of below applies.
        </p>
        <a href="/" className="mt-[5px] block">
          Please see the SEC website for more information.
        </a>
      </div>

      <div className="pt-4">
        <h6 className="mb-4">Other qualifiers</h6>

        <h6 className="mb-[5px]">Dealer</h6>
        <p>
          A dealer registered pursuant to section 15 of the Securities Exchange
          Act of 1934 (the “Exchange Act”), that in the aggregate owns and
          invests on a discretionary basis at least $10,000,000 of Eligible
          Securities (other than securities constituting the whole or a part of
          an unsold allotment to or subscription by a dealer as a participant in
          a public offering) acting for its own account or the accounts of other
          qualified institutional buyers.
        </p>

        <h6 className="my-[6px]">Dealer</h6>
        <p>
          A dealer registered pursuant to section 15 of the Exchange Act, acting
          in a riskless principal transaction on behalf of a qualified
          institutional buyer.
        </p>

        <h6 className="my-[6px]">Family of Investment Companies</h6>
        <p>
          An investment company registered under the Investment Company Act,
          acting for its own account or for the accounts of other qualified
          institutional buyers, that is part of a family of investment companies
          (as defined in Rule 144A) which own in the aggregate at least
          $100,000,000 in Eligible Securities.
        </p>

        <h6 className="my-[6px]">Other Entity</h6>
        <p>
          An entity, all of the equity owners of which are qualified
          institutional buyers, acting for its own account or the accounts of
          other qualified institutional buyers.
        </p>
      </div>
    </div>
  )
}

export default QIBDetails
