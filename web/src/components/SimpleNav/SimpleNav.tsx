import React from 'react'

import { Link, navigate, routes } from '@redwoodjs/router'

import { useAuth } from 'src/auth'

export const SimpleNav = () => {
  const { isAuthenticated, logOut } = useAuth()

  return (
    <nav className="bg-white shadow-sm">
      <div className="max-w-screen-8xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex justify-between items-center py-6">
          <Link to={routes.home()}>
            <img className="h-6 w-auto" src="/clade.wordmark.svg" alt="Clade" />
          </Link>
          {isAuthenticated && (
            <div>
              <button
                className="text-white bg-[#5021e9] hover:bg-indigo-500 px-4 py-2 rounded-md focus:outline-none focus:shadow-outline"
                onClick={() =>
                  logOut({}).then(() => {
                    navigate(routes.home())
                  })
                }
              >
                Logout
              </button>
            </div>
          )}
        </div>
      </div>
    </nav>
  )
}
