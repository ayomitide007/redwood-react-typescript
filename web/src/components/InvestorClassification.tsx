import React, { useState } from 'react';

const options = [
  {
    id: 'qib',
    title: 'Qualified Institutional Buyer (QIB)',
    description: 'An institutional investor that holds at least $100 million in non-affiliated securities; or A registered broker-dealer that holds at least $10 million in non-affiliated securities.',
    details: 'As defined in paragraph (a) of Rule 144A under the Securities Act of 1933...',
  },
  {
    id: 'qp',
    title: 'Qualified Purchaser (QP)',
    description: 'An investment manager with at least $25 million in investments; or A person or family office with at least $5 million in investments.',
    details: 'An investment manager registered under the Investment Company Act...',
  },
  {
    id: 'ai',
    title: 'Accredited Investor',
    description: 'Not a QIB or QP? Check to see if you qualify as an accredited investor.',
    details: 'A person or entity that meets the definition of an accredited investor under the Securities Act...',
  },
];

const InvestorClassification = () => {
  const [selectedOption, setSelectedOption] = useState<string | null>(null);
  const [detailsVisible, setDetailsVisible] = useState<string | null>(null);
  const [termsChecked, setTermsChecked] = useState(false);

  const handleSelectOption = (id: string) => {
    setSelectedOption(id);
    setDetailsVisible(id);
  };

  const handleToggleDetails = (id: string) => {
    setDetailsVisible(detailsVisible === id ? null : id);
  };

  return (
    <div className="max-w-4xl mx-auto p-6">
      <h2 className="text-2xl font-semibold mb-6">Choose your investment team’s status</h2>
      <div className="flex flex-col space-y-4">
        {options.map(option => (
          <div key={option.id} className={`border rounded-lg p-4 ${selectedOption === option.id ? 'border-indigo-500' : 'border-gray-300'}`}>
            <div className="flex justify-between items-center">
              <div>
                <h3 className="font-medium">{option.title}</h3>
                <p className="text-gray-600">{option.description}</p>
              </div>
              <div className="flex items-center space-x-2">
                <button
                  onClick={() => handleToggleDetails(option.id)}
                  className="text-indigo-600"
                >
                  {detailsVisible === option.id ? 'View less' : 'View more'}
                </button>
                <button
                  onClick={() => handleSelectOption(option.id)}
                  className={`px-4 py-2 rounded-lg ${selectedOption === option.id ? 'bg-indigo-600 text-white' : 'bg-indigo-100 text-indigo-600'}`}
                >
                  Select
                </button>
              </div>
            </div>
            {detailsVisible === option.id && (
              <div className="mt-4 bg-gray-100 p-4 rounded-lg">
                <p>{option.details}</p>
              </div>
            )}
          </div>
        ))}
      </div>
      <div className="mt-6 flex items-center space-x-2">
        <input
          type="checkbox"
          id="terms"
          checked={termsChecked}
          onChange={() => setTermsChecked(!termsChecked)}
        />
        <label htmlFor="terms" className="text-gray-600">I have read and agree to the <a href="#" className="text-indigo-600">Privacy Policy</a> and Terms of Use.</label>
      </div>
      <div className="mt-6 flex justify-between">
        <button className="px-4 py-2 bg-gray-300 rounded-lg" onClick={() => {/* handle back */}}>Back</button>
        <button
          className={`px-4 py-2 rounded-lg ${selectedOption && termsChecked ? 'bg-indigo-600 text-white' : 'bg-gray-300 text-gray-600'}`}
          disabled={!selectedOption || !termsChecked}
          onClick={() => {/* handle next */}}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default InvestorClassification;