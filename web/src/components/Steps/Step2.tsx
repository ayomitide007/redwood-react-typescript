import { FC, useState } from 'react'

import { InvestmentStatusEnum } from 'src/pages/StepperPage/StepperPage'

import AIDetails from '../InvestmentStatus/AIDetails'
import QIBDetails from '../InvestmentStatus/QIBDetails'
import QPDetails from '../InvestmentStatus/QPDetails'
import StatusCard from '../InvestmentStatus/StatusCard'

type Props = {
  selectedStatus: InvestmentStatusEnum
  setSelectedStatus: (status: InvestmentStatusEnum) => void
  isTermsAgreed: boolean
  setIsTermsAgreed: (isAgreed: boolean) => void
}

export const Step2: FC<Props> = ({
  selectedStatus,
  setSelectedStatus,
  isTermsAgreed,
  setIsTermsAgreed,
}) => {
  const [idOfViewMore, setIdOfViewMore] = useState('')

  const handleSelectStatus = (status: InvestmentStatusEnum) => {
    setSelectedStatus(status)
  }

  const handleViewMore = (id: string, isViewed: boolean) => {
    isViewed ? setIdOfViewMore(id) : setIdOfViewMore('')
  }

  const investmentStatuses = [
    {
      id: 'QIB',
      header: 'Qualified Institutional Buyer (QIB)',
      listItems: [
        'An institutional investor that holds at least $100 million in non-affiliated securities; or',
        'A registered broker-dealer that holds at least $10 million in non-affiliated securities.',
      ],
      moreInfo: <QIBDetails />,
    },
    {
      id: 'QP',
      header: 'Qualified Purchaser (QP)',
      listItems: [
        'An investment manager with at least $25 million in investments; or',
        'A person or family office with at least $5 million in investments.',
      ],
      moreInfo: <QPDetails />,
    },
    {
      id: 'AI',
      header: 'Accredited Investor',
      listItems: [
        'Not a QIB or QP? Check to see if you qualify as an accredited investor.',
      ],
      moreInfo: <AIDetails />,
    },
  ]

  return (
    <div className=" relative">
      <div className="px-6 py-4 text-left border-b border-solid border-[#DEE5EB]">
        <h5 className="text-xl font-normal text-[#17152F]">
          Choose your investment team’s status
        </h5>
      </div>

      <div className="p-6 text-left">
        <p className="text-[15px] text-[#17152F]">
          Select the option that applies:
        </p>

        <div className="my-5 flex flex-col gap-y-2 ">
          {investmentStatuses?.map((status) => (
            <StatusCard
              key={status?.id}
              id={status?.id}
              header={status?.header}
              listItems={status?.listItems}
              moreInfo={status?.moreInfo}
              handleSelectStatus={handleSelectStatus}
              isSelected={status?.id === selectedStatus}
              isMoreViewed={status?.id === idOfViewMore}
              handleViewMore={handleViewMore}
            />
          ))}
        </div>

        <label className="flex items-start gap-x-3">
          <input
            type="checkbox"
            className="mt-1 accent-[#5021E9] cursor-pointer"
            checked={isTermsAgreed}
            onChange={(ev) => setIsTermsAgreed(ev.target.checked)}
          />
          <p className="text-sm text-[#17152F]">
            I have read and agree to Clade’s{' '}
            <a href="/" className="text-[#5021E9]">
              Privacy Policy
            </a>{' '}
            and{' '}
            <a href="/" className="text-[#5021E9]">
              Terms of Use
            </a>{' '}
            . And if I am a resident of, or are located in, The European
            Economic Area, I consent to the transfer of my personal information
            as described in the Privacy Policy.
          </p>
        </label>
      </div>
    </div>
  )
}
