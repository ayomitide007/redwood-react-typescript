type Props = {
  backLabel?: string
  className?: string
  nextDisabled?: boolean
  nextLabel?: string
  numSteps: number
  onBack?: () => void
  onError?: () => void
  onNext?: () => void
  onRenderBack?: () => React.ReactNode
  onRenderNext?: () => React.ReactNode
  onSkip?: () => void
  onStep: number
  renderBack?: boolean
  renderNext?: boolean
  renderSkip?: boolean
  showSteps?: boolean
}

export const Navigation = ({
  backLabel = 'Back',
  className = '',
  nextDisabled = false,
  nextLabel = 'Next',
  numSteps,
  onBack = undefined,
  onError = () => {},
  onNext = undefined,
  onRenderBack = undefined,
  onRenderNext = undefined,
  onSkip,
  onStep,
  renderBack = true,
  renderNext = true,
  renderSkip = false,
  showSteps = true,
}: Props) => {
  const navigateBack = () => {
    if (onBack) {
      onBack()
    }
  }

  const navigateNext = () => {
    if (nextDisabled) {
      onError()
    } else if (onNext) {
      onNext()
    }
  }

  const renderBackButton = () => {
    if (!renderBack) {
      return null
    }

    if (onRenderBack) {
      return onRenderBack()
    }

    return (
      <div className="flex w-full sm:w-auto">
        <button
          className="w-full sm:w-auto bg-[#EBEBFF] hover:bg-indigo-300 text-[#5021e9] py-2 px-4 rounded-md focus:outline-none focus:shadow-inner"
          onClick={navigateBack}
        >
          {backLabel}
        </button>
      </div>
    )
  }

  const renderSteps = () => (
    <span className="text-sm text-gray-400 mx-4 uppercase w-full flex justify-center sm:w-auto">
      {showSteps && (
        <>
          Step {onStep} of {numSteps}
        </>
      )}
    </span>
  )

  const renderNextButton = () => {
    if (!renderNext) {
      return null
    }

    if (onRenderNext) {
      return onRenderNext()
    }

    return (
      <div className="flex w-full sm:w-auto">
        {renderSkip && (
          <button className="mr-4" onClick={onSkip}>
            Skip
          </button>
        )}
        <button
          className="w-full sm:w-auto bg-[#5021e9] hover:bg-indigo-300 text-white py-2 px-4 rounded-md focus:outline-none focus:shadow-outline disabled:cursor-not-allowed disabled:bg-[#C4C4C4] disabled:text-[#fff]"
          onClick={navigateNext}
          disabled={nextDisabled}
        >
          {nextLabel}
        </button>
      </div>
    )
  }

  return (
    <div className={className}>
      <div className="sm:hidden flex flex-col items-center space-y-5 px-5">
        {renderSteps()}
        {renderNextButton()}
        {renderBackButton()}
      </div>
      <div
        className={`hidden sm:flex ${
          !renderBack && !renderNext
            ? 'sm:justify-center'
            : 'sm:justify-between'
        } sm:w-full sm:items-center`}
      >
        {renderBackButton()}
        {renderSteps()}
        {renderNextButton()}
      </div>
    </div>
  )
}

export default Navigation
