// Pass props to your component by passing an `args` object to your story
//
// ```jsx
// export const Primary: Story = {
//  args: {
//    propName: propValue
//  }
// }
// ```
//
// See https://storybook.js.org/docs/react/writing-stories/args.

import React from 'react'
import Navigation from './Navigation'

export default {
  component: Navigation,
  title: 'Components/Navigation',
  decorators: [(story) => <div style={{ padding: '3rem' }}>{story()}</div>],
  tags: ['autodocs'],
}

export const Default = {
  args: {
    numSteps: 5,
    onStep: 3,
    onBack: () => console.log('navigated back'),
    onNext: () => console.log('navigated next'),
    onError: () => console.log('error triggered'),
    onSkip: () => console.log('skipped'),
  },
}

export const WithSkip = {
  args: {
    ...Default.args,
    renderSkip: true,
  },
}

export const NextDisabled = {
  args: {
    ...Default.args,
    nextDisabled: true,
  },
}

export const NoBackButton = {
  args: {
    ...Default.args,
    renderBack: false,
  },
}

export const NoNextButton = {
  args: {
    ...Default.args,
    renderNext: false,
  },
}

// Add more stories as needed to showcase other variations of the component.
