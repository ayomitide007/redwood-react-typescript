import { render } from '@redwoodjs/testing/web'

import Navigation from './Navigation'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('Navigation', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<Navigation numSteps={1} onStep={1} />)
    }).not.toThrow()
  })
})
