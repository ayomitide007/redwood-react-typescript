import { render } from '@redwoodjs/testing/web'

import StepperPage from './StepperPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('StepperPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<StepperPage />)
    }).not.toThrow()
  })
})
