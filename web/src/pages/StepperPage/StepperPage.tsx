import { useState } from 'react'

import { Redirect, navigate, routes } from '@redwoodjs/router'
import { MetaTags } from '@redwoodjs/web'

import {
  Navigation,
  Step1,
  Step2,
  Step3,
  SteppedProgress,
} from 'src/components'
import StepperLayout from 'src/layouts/StepperLayout/StepperLayout'

const StepperPage = ({ step }) => {
  const [selectedStatus, setSelectedStatus] =
    useState<InvestmentStatusEnum>(null)

  const [isTermsAgreed, setIsTermsAgreed] = useState(false)

  if (!step) {
    return (
      <Redirect
        to={routes.stepperPage({ step: 1 })}
        options={{ replace: true }}
      />
    )
  }

  const stepsMap = [
    'Company Details',
    'Investor Classification',
    'Investment Preference',
  ]

  const ComponentForStep = [
    () => <Step1 />,
    () => (
      <Step2
        selectedStatus={selectedStatus}
        setSelectedStatus={setSelectedStatus}
        isTermsAgreed={isTermsAgreed}
        setIsTermsAgreed={setIsTermsAgreed}
      />
    ),
    () => <Step3 />,
  ]

  const navigateBack = () => {
    if (step > 1) {
      navigate(routes.stepperPage({ step: step - 1 }))
    }
  }

  const navigateNext = () => {
    if (step < 3) {
      navigate(routes.stepperPage({ step: step + 1 }))
    }
  }

  const renderProgress = () => {
    return (
      <div className="hidden sm:block pt-8 pb-12">
        <SteppedProgress
          steps={[1, 2, 3]}
          activeStep={step}
          mapText={(step) => stepsMap[step - 1]}
        />
      </div>
    )
  }

  const isNextDisabled = () => {
    if (step === 2) {
      return !selectedStatus || !isTermsAgreed
    }
    return false
  }

  return (
    <StepperLayout renderProgress={renderProgress}>
      <MetaTags title="Stepper" description="Stepper page" />

      <div className="w-full h-full flex flex-col">
        {ComponentForStep[step - 1]()}
        <Navigation
          className="sm:border-t mt-14 sm:mt-0 px-0 sm:px-6 pb-5 pt-5 sm:pb-5"
          numSteps={3}
          onBack={navigateBack}
          onNext={navigateNext}
          onStep={step}
          nextDisabled={isNextDisabled()}
        />
      </div>
    </StepperLayout>
  )
}

export default StepperPage

export enum InvestmentStatusEnum {
  QIB = 'QIB',
  QP = 'QP',
  AI = 'AI',
}
