import Helmet from 'react-helmet'

import { SimpleNav } from 'src/components'

const StepperLayout = ({
  children,
  card = true,
  loading = false,
  renderProgress = undefined,
}) => {
  return (
    <div>
      <Helmet>
        <title>Clade</title>
      </Helmet>
      <div className="grid grid-rows-[auto,1fr] min-h-screen bg-gray-100">
        <SimpleNav />
        <DivWrap renderProgress={renderProgress}>
          {renderProgress?.()}
          {/* <main className="grid grid-cols-1 justify-center sm:justify-items-center sm:items-center pt-3 sm:pt-0 sm:m-8"> */}
          {/* <div
              className={`${
                card ? 'rounded-lg max-w-xl' : ''
              } overflow-visible mx-0`}
            > */}
          <main className="flex my-[60px] justify-center w-full">
            <div
              className={`overflow-visible w-[448px] max-w-full ${
                loading ? 'bg-gray-100' : 'bg-white shadow'
              } rounded-lg `}
            >
              {children}
            </div>
          </main>
          {/* </div> */}
          {/* </main> */}
        </DivWrap>
      </div>
    </div>
  )
}

export default StepperLayout

const DivWrap = ({ renderProgress, children }) => {
  if (renderProgress?.()) {
    return <div className="w-screen">{children}</div>
  } else {
    return <>{children}</>
  }
}
