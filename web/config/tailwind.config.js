module.exports = {
  content: ['src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      screens: {
        '8xl': '1533px',
      },
    },
  },
  plugins: [],
}
